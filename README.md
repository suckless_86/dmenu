# Personal build of dmenu **(2023)**

*Patches Included*
- [border](https://tools.suckless.org/dmenu/patches/border/)
- [center](https://tools.suckless.org/dmenu/patches/center/)

